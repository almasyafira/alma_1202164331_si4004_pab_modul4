package com.example.android.eadrestaurant;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class login extends AppCompatActivity {

    public TextView daftar;
    EditText email, pass;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.txtemail);
        pass = findViewById(R.id.txtpass);
        mAuth = FirebaseAuth.getInstance();

        daftar = findViewById(R.id.txtdaftar);
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login.this, registrasi.class);
                login.this.startActivity(intent);

            }
        });
    }

    public boolean check(){
        if (email.getText().toString().equals("")){
            email.setError("Isi email");
            email.requestFocus();
            return false;
        }
        if (pass.getText().toString().equals("")){
            pass.setError("Isi password");
            pass.requestFocus();
            return false;
        }
        return true;
    }

    public void masuk(View view){
        if (check()) {
            new AsyncTask<Void,Void,Boolean>(){
                @Override
                protected Boolean doInBackground(Void... voids) {
                    mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        startActivity(new Intent(login.this, MainActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(login.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    return null;
                }

                @Override
                protected void onPreExecute() {
                    Toast.makeText(login.this, "Sign In...", Toast.LENGTH_SHORT).show();

                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                }
            }.execute();
        }
    }
}
